#ifndef ButtonStyle_h__
#define ButtonStyle_h__

#include  "guiStyle.h"

#include <dlib/spriteRenderer.h>
#include <dlib/texture2D.h>
#include <dlib/characterSet.h>

#include <glm/vec4.hpp>

namespace DGUI
{
	struct ButtonStyle
			: public DGUI::GUIStyle
	{
		enum class INDICIES
		{
			NORMAL = 0, HOVER, CLICK
		};

		ButtonStyle()
			: characterSet(nullptr)
		{
			backgroundColors[static_cast<int>(INDICIES::NORMAL)] = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
			backgroundColors[static_cast<int>(INDICIES::HOVER)] = textColors[static_cast<int>(INDICIES::NORMAL)];
			backgroundColors[static_cast<int>(INDICIES::CLICK)] = textColors[static_cast<int>(INDICIES::NORMAL)];

			textColors[static_cast<int>(INDICIES::NORMAL)] = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
			textColors[static_cast<int>(INDICIES::HOVER)] = textColors[static_cast<int>(INDICIES::NORMAL)];
			textColors[static_cast<int>(INDICIES::CLICK)] = textColors[static_cast<int>(INDICIES::NORMAL)];
		};

		~ButtonStyle()
		{ };

		DLib::CharacterSet* characterSet;

		glm::vec4 backgroundColors[3];
		glm::vec4 textColors[3];

		glm::vec2 textOffset;
	};
}

#endif // ButtonStyle_h__
