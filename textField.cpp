#include "textField.h"

#include <dlib/logger.h>
#include <dlib/input.h>

#include <utf8-cpp/utf8.h>

DGUI::TextField::TextField()
	: GUIContainer()
    , allowEdit(true)
    , textColor(0.0f, 0.0f, 0.0f, 1.0f)
	, drawCursor(false)
    , mouseDown(false)
    , cursorIndex(0)
    , cursorRowIndex(0)
	, selectionIndex(-1)
	, selectionStartIndex(-1)
	, selectionEndIndex(-1)
	, selectionStartStringIndex(-1)
	, selectionEndStringIndex(-1)
	, visibleStringsBegin(0)
	, visibleStringsEnd(0)
	, matchWidth(-1)
{
	active = false;
}

DGUI::TextField::~TextField()
{
}

void DGUI::TextField::Init(const DLib::Rect& area, GUIStyle* style, GUIBackground* background, DGUI::GUIStyle* backgroundStyle)
{
	this->style = CastStyleTo<TextFieldStyle>(style, DLib::LOG_TYPE_ERROR);
	if(this->style == nullptr)
		return;
	else if(this->style->characterSet == nullptr)
	{
		DLib::Logger::LogLine(DLib::LOG_TYPE_ERROR, "TextFieldStyle->characterSet == nullptr!");
		return;
	}

	GUIContainer::Init(area
					   , style
					   , background
					   , backgroundStyle);
	
	cursorColor = this->style->cursorColorNormal;

	scrollbar.Init(area
				   , this->style->scrollBarStyle
				   , this->style->scrollbarBackground
				   , this->style->scrollbarBackgroundStyle);

	SetCharacterSet(this->style->characterSet);
}

void DGUI::TextField::Update(std::chrono::nanoseconds delta)
{
	scrollbar.Update(delta);

	if(mouseDown && DLib::Input::MouseMoved())
	{
		if(scrollbar.GetActive())
		{
			UpdateVisibleStrings();
		}
		else
		{
			//TODO: Implement selection
		}
	}
}

void DGUI::TextField::DrawBackground(DLib::SpriteRenderer* spriteRenderer)
{
	background->Draw(spriteRenderer);
	//spriteRenderer->Draw(area, style->backgroundColorNormal);

	glm::vec2 drawPos = area.GetMinPosition();

	if(SelectionMade())
	{
		drawPos.y += style->characterSet->GetLineHeight() * (std::max(selectionStartStringIndex, visibleStringsBegin) - visibleStringsBegin);

		int baseWidth = style->characterSet->GetWidthAtIndex(constructedStrings[std::get<0>(drawingData[selectionStartStringIndex])]
														 , std::get<1>(drawingData[selectionStartStringIndex]));

		int widthAtSelectionStart = style->characterSet->GetWidthAtIndex(constructedStrings[std::get<0>(drawingData[selectionStartStringIndex])]
											  , std::get<1>(drawingData[selectionStartStringIndex]) + selectionStartIndex) - baseWidth;

		//If the selection starts and ends at the same line only one rectangle has to be drawn
		//Otherwise two or three rectangles have to be drawn
		if(selectionStartStringIndex == selectionEndStringIndex)
		{
			//There is no need to do anything if the selected line isn't visible
			if(selectionStartStringIndex >= visibleStringsBegin
			   && selectionEndStringIndex < visibleStringsEnd)
			{
				int drawEnd = style->characterSet->GetWidthAtIndex(constructedStrings[std::get<0>(drawingData[selectionStartStringIndex])]
															, std::get<1>(drawingData[selectionStartStringIndex]) + selectionEndIndex) - baseWidth;

				spriteRenderer->Draw(DLib::Rect(drawPos.x + widthAtSelectionStart
												, drawPos.y
												, drawEnd - widthAtSelectionStart
												, style->characterSet->GetLineHeight())
									 , style->textHighlightColor);
			}
		}
		else
		{
			//First (mixed) row
			if(selectionStartStringIndex >= visibleStringsBegin
			   && selectionStartStringIndex < visibleStringsEnd)
			{
				spriteRenderer->Draw(DLib::Rect(drawPos.x + widthAtSelectionStart
												, drawPos.y
												, area.GetWidth() - widthAtSelectionStart
												, style->characterSet->GetLineHeight())
									 , style->textHighlightColor);

				drawPos.y += style->characterSet->GetLineHeight();
			}

			//Middle
			if(abs(selectionStartStringIndex - selectionEndStringIndex) > 1)
			{
				int minVal = std::min(selectionEndStringIndex, visibleStringsEnd);
				int maxVal = std::max(selectionStartStringIndex + 1, visibleStringsBegin);

				if(minVal - maxVal > 0)
				{

					int drawPosInc = style->characterSet->GetLineHeight()
									 * (minVal - maxVal);

					spriteRenderer->Draw(DLib::Rect(drawPos
													, area.GetWidth()
													, drawPosInc)
										 , style->textHighlightColor);

					drawPos.y += drawPosInc;
				}
			}

			//Last (mixed) row
			if(selectionEndStringIndex >= visibleStringsBegin
			   && selectionEndStringIndex < visibleStringsEnd)
			{
				int widthAtEndStringIndex = style->characterSet->GetWidthAtIndex(constructedStrings[std::get<0>(drawingData[selectionEndStringIndex])]
																		  , std::get<1>(drawingData[selectionEndStringIndex]));

				int drawEnd = style->characterSet->GetWidthAtIndex(constructedStrings[std::get<0>(drawingData[selectionEndStringIndex])]
															,
						std::get<1>(drawingData[selectionEndStringIndex]) + selectionEndIndex);

				spriteRenderer->Draw(DLib::Rect(drawPos.x
												, drawPos.y
												, drawEnd - widthAtEndStringIndex
												, style->characterSet->GetLineHeight())
									 , style->textHighlightColor);
			}
		}
	}

	scrollbar.DrawBackground(spriteRenderer);
}

void DGUI::TextField::DrawMiddle(DLib::SpriteRenderer* spriteRenderer)
{
	glm::vec2 drawPos = area.GetMinPosition();

	for(auto beginIter = drawingData.begin() + visibleStringsBegin,
				endIter = drawingData.begin() + visibleStringsEnd; beginIter != endIter; ++beginIter)
	{
		auto tuple = *beginIter;

		spriteRenderer->DrawString(style->characterSet, constructedStrings[std::get<0>(tuple)], drawPos, std::get<1>(tuple), std::get<2>(tuple), style->textColorNormal);

		drawPos.y += style->characterSet->GetLineHeight();
	}

	//scrollbar.DrawMiddle(spriteRenderer);
}


void DGUI::TextField::DrawForeground(DLib::SpriteRenderer* spriteRenderer)
{
	if(drawCursor)
	{
		if(cursorBlinkTimer.GetTime() >= cursorBlinkTime)
		{
			cursorBlinkTimer.Reset();
			cursorColor = (cursorColor == style->cursorColorNormal ? style->cursorColorBlink : style->cursorColorNormal);
		}

		spriteRenderer->Draw(DLib::Rect(cursorPosition + style->cursorOffset, style->cursorSize), cursorColor);
	}

	//scrollbar.DrawForeground(spriteRenderer);
}

void DGUI::TextField::AddText(const std::string& text)
{
	int newEntries = 0;

	constructedStrings.emplace_back(style->characterSet->ConstructString(text));

	int width = 0;
	int drawStartIndex = 0;
	int drawCount = 0;

	for(const DLib::CharacterBlock& block : constructedStrings.back().characterBlocks)
	{
		if(width + block.width <= area.GetWidth() - scrollbar.GetSize().x - style->scrollBarPadding)
		{
			drawCount += block.utf8Length + 1;
			width += block.width + style->characterSet->GetSpaceXAdvance();
		}
		else
		{
			if(block.width > area.GetWidth() - scrollbar.GetSize().x - style->scrollBarPadding)
			{
				//Block needs to be split into several lines

				for(const DLib::Character* character : block.characters)
				{
					if(width + character->xAdvance < area.GetWidth() - scrollbar.GetSize().x - style->scrollBarPadding)
					{
						width += character->xAdvance;
						drawCount++;
					}
					else
					{
						drawingData.emplace_back(static_cast<int>(constructedStrings.size() - 1), drawStartIndex, drawCount);
						newEntries++;

						width = character->xAdvance;
						drawStartIndex += drawCount;
						drawCount = 1;
					}
				}

				width += style->characterSet->GetSpaceXAdvance();
				drawCount++;
			}
			else
			{
				//Block will fit on a line

				drawingData.emplace_back(static_cast<int>(constructedStrings.size() - 1), drawStartIndex, drawCount - 1); //Skip space
				newEntries++;

				width = block.width + style->characterSet->GetSpaceXAdvance();
				drawStartIndex += drawCount;
				drawCount = block.utf8Length + 1;
			}
		}
	}

	drawingData.emplace_back(static_cast<int>(constructedStrings.size() - 1), drawStartIndex, drawCount - 1); //Skip last space
	newEntries++;

#ifdef _DEBUG
	if(drawingData.size() > 2)
	{
		int zeroA = std::get<0>(*(drawingData.end() - 1));
		int zeroB = std::get<0>(*(drawingData.end() - 2));

		int oneA = std::get<1>(*(drawingData.end() - 1));
		int oneB = std::get<1>(*(drawingData.end() - 2));

		assert(!((zeroA == zeroB) && (oneA == oneB)));
	}
#endif

	scrollbar.SetMaxItems(scrollbar.GetMaxItems() + newEntries);

	UpdateVisibleStrings();
}

std::vector<DLib::ConstructedString> DGUI::TextField::GetText()
{
	return constructedStrings;
}

std::string DGUI::TextField::GetText(int stringIndex)
{
	return constructedStrings[stringIndex].text;
}

std::string DGUI::TextField::GetSelectedText()
{
	if(selectionStartStringIndex == selectionEndStringIndex)
	{
		return GetCurrentConstructedString().text.substr(GetCurrentDrawIndex() + selectionStartIndex, selectionEndIndex - selectionStartIndex);
	}
	else
	{
		std::string returnString("");

		int oldConstructedStringIndex = std::get<0>(drawingData[selectionStartStringIndex]);
		returnString += constructedStrings[oldConstructedStringIndex].text.substr(std::get<1>(drawingData[selectionStartStringIndex]) + selectionStartIndex, std::get<2>(drawingData[selectionStartStringIndex]) - selectionStartIndex);
		
		int newConstructedStringIndex = 0;

		for(int i = selectionStartStringIndex + 1; i < selectionEndStringIndex; i++)
		{
			newConstructedStringIndex = std::get<0>(drawingData[i]);

			if(newConstructedStringIndex != oldConstructedStringIndex)
				returnString += '\n';

			returnString += constructedStrings[newConstructedStringIndex].text.substr(std::get<1>(drawingData[i]), std::get<2>(drawingData[i]));

			oldConstructedStringIndex = newConstructedStringIndex;
		}

		newConstructedStringIndex = std::get<0>(drawingData[selectionEndStringIndex]);

		if(newConstructedStringIndex != oldConstructedStringIndex)
			returnString += '\n';

		returnString += constructedStrings[std::get<0>(drawingData[selectionEndStringIndex])].text.substr(std::get<1>(drawingData[selectionEndStringIndex]), selectionEndIndex);

		return returnString;
	}

	return ""; //This shouldn't happen, but it stops some compilers from complaining
}

void DGUI::TextField::SetCharacterSet(const std::string& fontPath, DLib::ContentManager* contentManager)
{
	SetCharacterSet(contentManager->Load<DLib::CharacterSet>(fontPath));
}

void DGUI::TextField::SetCharacterSet(const DLib::CharacterSet* characterSet)
{
	this->style->characterSet = style->characterSet;

	for(DLib::ConstructedString& constructedString : constructedStrings)
		constructedString = this->style->characterSet->ConstructString(constructedString.text);

	visibleStringsMax = static_cast<int>(ceil(area.GetHeight() / style->characterSet->GetLineHeight()));
	scrollbar.SetVisibleItems(visibleStringsMax);
}

void DGUI::TextField::SetCutoffString(const std::string& string)
{
	constructedCutoffString = style->characterSet->ConstructString(string);
}

unsigned int DGUI::TextField::GetCursorIndex(int width, int line)
{
	int widthAtIndex = style->characterSet->GetWidthAtIndex(constructedStrings[std::get<0>(drawingData[line])], std::get<1>(drawingData[line]));

	int indexAtWidth = style->characterSet->GetIndexAtWidth(constructedStrings[std::get<0>(drawingData[line])], widthAtIndex + width);

	return indexAtWidth - std::get<1>(drawingData[line]);
}

void DGUI::TextField::OnKeyDown(const DLib::KeyState& keyState)
{
	if(active)
	{
		cursorBlinkTimer.Reset();
		cursorColor = style->cursorColorNormal;

		switch(keyState.key)
		{
			case GLFW_KEY_UP:
				UpPressed(keyState);
				break;
			case GLFW_KEY_DOWN:
				DownPressed(keyState);
				break;
			case GLFW_KEY_LEFT:
				LeftPressed(keyState);
				break;
			case GLFW_KEY_RIGHT:
				RightPressed(keyState);
				break;
			/*case GLFW_KEY_X:
				if(keyState.mods == GLFW_MOD_CONTROL)
				{
					glfwSetClipboardString(DLib::Input::GetListenWindow(), GetSelectedText().c_str());

					if(allowEdit)
					{
						EraseSelection();
						SetXOffset();
					}
				}
				break;*/
			case GLFW_KEY_C:
				if(keyState.mods == GLFW_MOD_CONTROL)
				{
					std::string selectedText = GetSelectedText();
					glfwSetClipboardString(DLib::Input::GetListenWindow(), GetSelectedText().c_str());
				}
				break;
			case GLFW_KEY_V:
				if(keyState.mods == GLFW_MOD_CONTROL)
				{
					const char* text = glfwGetClipboardString(DLib::Input::GetListenWindow());
					std::string stringText(text);

					/*if(text != NULL
					   && allowEdit)
					{
						Insert(cursorIndex, text);
						SetXOffset();
					}*/
				}
				break;
			case GLFW_KEY_A:
				if(keyState.mods == GLFW_MOD_CONTROL)
				{
					cursorRowIndex = 0;
					cursorIndex = 0;

					BeginSelection();

					cursorRowIndex = drawingData.size() - 1;
					SetCursorIndex(GetCurrentDrawCount());

					ExtendSelectionToCursor();
				}
				break;
			case GLFW_KEY_HOME:
				HomePressed(keyState);
				break;
			case GLFW_KEY_END:
				EndPressed(keyState);
				break;
			default:
				break;
		}
	}
}

void DGUI::TextField::OnMouseEnter()
{
	active = true;
}

void DGUI::TextField::OnMouseExit()
{
}

void DGUI::TextField::OnMouseDown(const DLib::KeyState& keyState, const glm::vec2& mousePosition)
{
	if(drawingData.size() > 0) //TODO: This is a pretty bad fix
	{
		if(keyState.key == GLFW_MOUSE_BUTTON_LEFT)
		{
			matchWidth = -1;

			if(keyState.action == GLFW_PRESS)
			{
				if(!area.Contains(mousePosition))
				{
					active = false;

					drawCursor = false;
				}
				else if(!scrollbar.Contains(mousePosition))
				{
					cursorBlinkTimer.Reset();
					cursorBlinkTimer.Start();

					cursorColor = style->cursorColorNormal;

					unsigned int rowIndex = static_cast<unsigned int>((
							(mousePosition.y - area.GetMinPosition().y) / style->characterSet->GetLineHeight() +
							visibleStringsBegin));

					if(keyState.mods == GLFW_MOD_SHIFT)
					{
						if(!SelectionMade())
							BeginSelection();

						SetRowIndex(rowIndex);
						SetCursorIndex(GetCursorIndex(static_cast<int>(mousePosition.x - area.GetMinPosition().x)
													  , cursorRowIndex));
						ExtendSelectionToCursor();
					}
					else
					{
						Deselect();
						SetRowIndex(rowIndex);
						SetCursorIndex(GetCursorIndex(static_cast<int>(mousePosition.x - area.GetMinPosition().x)
													  , cursorRowIndex));
					}

					matchWidth = static_cast<int>(cursorPosition.x - area.GetMinPosition().x);

					mouseDown = true;
				}
				else
				{
					mouseDown = true;
				}
			}
			else if(keyState.action == GLFW_DOUBLE_CLICK)
			{
				if(!SelectionMade())
				{
					JumpCursorLeft();
					BeginSelection();
					JumpCursorRight();
					ExtendSelectionToCursor();
				}
			}
		}

		if(mouseDown)
			scrollbar.OnMouseDown(keyState
								  , mousePosition);
	}
}

void DGUI::TextField::OnMouseUp(const DLib::KeyState& keyState, const glm::vec2& mousePosition)
{
	if(keyState.key == GLFW_MOUSE_BUTTON_LEFT)
	{
		mouseDown = false;
		scrollbar.OnMouseUp(keyState, mousePosition);

		if(!area.Contains(mousePosition))
			active = false;
	}
}

void DGUI::TextField::OnScroll(double x, double y)
{
	scrollbar.Scroll(static_cast<int>(-y));

	UpdateVisibleStrings();
}

void DGUI::TextField::SetPosition(const glm::vec2& newPosition)
{
	SetPosition(newPosition.x, newPosition.y);
}

void DGUI::TextField::SetPosition(float x, float y)
{
	GUIContainer::SetPosition(x, y);

	scrollbar.SetPosition(area.GetMaxPosition().x, area.GetMinPosition().y);
}

void DGUI::TextField::SetSize(const glm::vec2& newSize)
{
	SetSize(newSize.x, newSize.y);
}

void DGUI::TextField::SetSize(float x, float y)
{
	float xDelta = area.GetWidth() - x;

	GUIContainer::SetSize(x, y);

	scrollbar.SetSize(glm::vec2(scrollbar.GetSize().x, y));
	scrollbar.SetVisibleItems(ceil(area.GetHeight() / style->characterSet->GetLineHeight()));

	if(xDelta != 0)
	{
		scrollbar.SetPosition(area.GetMaxPosition().x, area.GetMinPosition().y);
		UpdateText();
	}
}

void DGUI::TextField::UpdateText()
{
	drawingData.clear();

	int i = 0;
	for(const DLib::ConstructedString& constructedString : constructedStrings)
	{
		int width = 0;
		int drawStartIndex = 0;
		int drawCount = 0;

		for(const DLib::CharacterBlock& block : constructedString.characterBlocks)
		{
			if(width + block.width <= area.GetWidth() - scrollbar.GetSize().x)
			{
				drawCount += block.utf8Length + 1;
				width += block.width + style->characterSet->GetSpaceXAdvance();
			}
			else
			{
				if(block.width > area.GetWidth() - scrollbar.GetSize().x)
				{
					for(const DLib::Character* character : block.characters)
					{
						if(width + character->xAdvance < area.GetWidth() - scrollbar.GetSize().x)
						{
							width += character->xAdvance;
							drawCount++;
						}
						else
						{
							drawingData.emplace_back(i, drawStartIndex, drawCount);

							width = character->xAdvance;
							drawStartIndex += drawCount;
							drawCount = 1;
						}
					}

					width += style->characterSet->GetSpaceXAdvance();
					drawCount++;
				}
				else
				{
					drawingData.emplace_back(i, drawStartIndex, drawCount);

					width = block.width + style->characterSet->GetSpaceXAdvance();
					drawStartIndex += drawCount;
					drawCount = block.utf8Length + 1;
				}
			}
		}
		
		drawingData.emplace_back(i, drawStartIndex, drawCount);

#ifdef _DEBUG //_DEBUG Visual Studio only?
		if(drawingData.size() > 2)
		{
			int zeroA = std::get<0>(*(drawingData.end() - 1));
			int zeroB = std::get<0>(*(drawingData.end() - 2));

			int oneA = std::get<1>(*(drawingData.end() - 1));
			int oneB = std::get<1>(*(drawingData.end() - 2));

			assert(!((zeroA == zeroB) && (oneA == oneB)));
		}
#endif

		i++;
	}

	scrollbar.SetMaxItems(drawingData.size());
	UpdateVisibleStrings();
}

void DGUI::TextField::UpPressed(const DLib::KeyState& keyState)
{
	if(matchWidth == -1)
		matchWidth = cursorPosition.x - area.GetMinPosition().x;

	switch(keyState.mods)
	{
		case 0:
			if(SelectionMade())
				Deselect();

			MoveCursorUp();
			scrollbar.ScrollTo(cursorRowIndex);
			break;
		case GLFW_MOD_SHIFT:
			if(!SelectionMade())
				BeginSelection();

			MoveCursorUp();
			scrollbar.ScrollTo(cursorRowIndex);
			ExtendSelectionToCursor();
			break;
		case GLFW_MOD_CONTROL:
			scrollbar.Scroll(-1);
			UpdateVisibleStrings();
			break;
		default:
			break;
	}

}

void DGUI::TextField::DownPressed(const DLib::KeyState& keyState)
{
	if(matchWidth == -1)
		matchWidth = cursorPosition.x - area.GetMinPosition().x;

	switch(keyState.mods)
	{
		case 0:
			if(SelectionMade())
				Deselect();

			MoveCursorDown();
			scrollbar.ScrollTo(cursorRowIndex);
			break;
		case GLFW_MOD_SHIFT:
			if(!SelectionMade())
				BeginSelection();

			MoveCursorDown();
			scrollbar.ScrollTo(cursorRowIndex);
			ExtendSelectionToCursor();
			break;
		case GLFW_MOD_CONTROL:
			scrollbar.Scroll(1);
			UpdateVisibleStrings();
			break;
		default:
			break;
	}
}

void DGUI::TextField::LeftPressed(const DLib::KeyState& keyState)
{
	matchWidth = -1;

	switch(keyState.mods)
	{
		case 0:
			if(!SelectionMade())
				MoveCursorLeft();
			else
			{
				SetRowIndex(selectionStartStringIndex);
				SetCursorIndex(selectionStartIndex);
				Deselect();
			}
			break;
		case GLFW_MOD_SHIFT:
			if(!SelectionMade())
				BeginSelection();

			MoveCursorLeft();
			ExtendSelectionToCursor();
			break;
		case GLFW_MOD_CONTROL:
			if(SelectionMade())
				Deselect();

			JumpCursorLeft();
			break;
		case GLFW_MOD_SHIFT | GLFW_MOD_CONTROL:
			if(!SelectionMade())
				BeginSelection();

			JumpCursorLeft();
			ExtendSelectionToCursor();
			break;
		default:
			break;
	}
}

void DGUI::TextField::RightPressed(const DLib::KeyState& keyState)
{
	matchWidth = -1;

	switch(keyState.mods)
	{
		case 0:
			if(!SelectionMade())
			{
				MoveCursorRight();
			}
			else
			{
				SetRowIndex(selectionEndStringIndex);

				SetCursorIndex(selectionEndIndex);
				Deselect();
			}
			break;
		case GLFW_MOD_SHIFT:
			if(!SelectionMade())
				BeginSelection();

			MoveCursorRight();
			ExtendSelectionToCursor();
			break;
		case GLFW_MOD_CONTROL:
			if(SelectionMade())
				Deselect();

			JumpCursorRight();
			break;
		case GLFW_MOD_SHIFT | GLFW_MOD_CONTROL:
			if(!SelectionMade())
				BeginSelection();

			JumpCursorRight();
			ExtendSelectionToCursor();
			break;
		default:
			break;
	}
}

void DGUI::TextField::BackspacePressed(const DLib::KeyState& keyState)
{
	//if(keyState.mods == GLFW_MOD_CONTROL)
	//{
	//	BeginSelection();
	//	JumpCursorLeft();
	//	ExtendSelectionToCursor();
	//	EraseSelection();
	//}
	//else
	//{
	//	if(SelectionMade())
	//		EraseSelection();
	//	else
	//	{
	//		auto iter = constructedString.text.begin();
	//		utf8::unchecked::advance(iter, cursorIndex - 1); //I just discovered advance(). Horray for me...

	//		style->characterSet->Erase(constructedString, cursorIndex - 1, 1);

	//		MoveCursorLeft();
	//		unsigned int index = cursorIndex;

	//		JumpCursorLeft();
	//		SetXOffset();
	//		SetCursorIndex(index);
	//	}
	//}

	//SetXOffset();
}

void DGUI::TextField::DeletePressed(const DLib::KeyState& keyState)
{
	/*if(keyState.mods == GLFW_MOD_CONTROL)
	{
		BeginSelection();
		JumpCursorRight();
		ExtendSelectionToCursor();
		EraseSelection();
	}
	else
	{
		if(SelectionMade())
			EraseSelection();
		else
			style->characterSet->Erase(constructedString, cursorIndex, 1);
	}

	SetXOffset();*/
}

void DGUI::TextField::HomePressed(const DLib::KeyState & keyState)
{
	matchWidth = -1;

	switch(keyState.mods)
	{
		case 0:
			Deselect();
			SetCursorIndex(0);
			break;
		case GLFW_MOD_SHIFT:
			if(!SelectionMade())
				BeginSelection();

			SetCursorIndex(0);
			ExtendSelectionToCursor();
			break;
		case GLFW_MOD_CONTROL:
			cursorRowIndex = 0;
			SetCursorIndex(0);
			break;
		case GLFW_MOD_SHIFT | GLFW_MOD_CONTROL:
			if(!SelectionMade())
				BeginSelection();

			cursorRowIndex = 0;
			SetCursorIndex(0);
			ExtendSelectionToCursor();
			break;
		default:
			break;
	}
}

void DGUI::TextField::EndPressed(const DLib::KeyState & keyState)
{
	matchWidth = -1;

	switch(keyState.mods)
	{
		case 0:
			Deselect();
			SetCursorIndex(GetCurrentDrawCount());
			break;
		case GLFW_MOD_SHIFT:
			if(!SelectionMade())
				BeginSelection();

			SetCursorIndex(GetCurrentDrawCount());
			ExtendSelectionToCursor();
			break;
		case GLFW_MOD_CONTROL:
			cursorRowIndex = drawingData.size() - 1;
			SetCursorIndex(GetCurrentDrawCount());
			break;
		case GLFW_MOD_SHIFT | GLFW_MOD_CONTROL:
			if(!SelectionMade())
				BeginSelection();

			cursorRowIndex = drawingData.size() - 1;
			SetCursorIndex(GetCurrentDrawCount());
			ExtendSelectionToCursor();
			break;
		default:
			break;
	}
}

void DGUI::TextField::SetCursorIndex(unsigned int newIndex)
{
	//Unsigned int means there's no need to check values below 0
	scrollbar.ScrollTo(cursorRowIndex);
	UpdateVisibleStrings();

	if(newIndex > GetCurrentDrawCount())
		newIndex = GetCurrentDrawCount();

	cursorIndex = newIndex;

	//I see nothing wrong with this line
	cursorPosition.x = area.GetMinPosition().x + style->characterSet->GetWidthAtIndex(constructedStrings[std::get<0>(drawingData[cursorRowIndex])], GetCurrentDrawIndex() + cursorIndex) - style->characterSet->GetWidthAtIndex(constructedStrings[std::get<0>(drawingData[cursorRowIndex])], GetCurrentDrawIndex());
}

void DGUI::TextField::SetRowIndex(unsigned int newIndex)
{
	if(newIndex >= drawingData.size())
		newIndex = drawingData.size() - 1;
	
	cursorRowIndex = newIndex;

	scrollbar.ScrollTo(newIndex);
	UpdateVisibleStrings();
}

void DGUI::TextField::MoveCursorUp()
{
	if(cursorRowIndex > 0)
	{
		cursorRowIndex--;

		if(cursorRowIndex < visibleStringsBegin)
		{
			scrollbar.Scroll(-1);

			UpdateVisibleStrings();
		}
		
		unsigned int widthAtIndex = style->characterSet->GetWidthAtIndex(GetCurrentConstructedString(), GetCurrentDrawIndex());
		SetCursorIndex(style->characterSet->GetIndexAtWidth(GetCurrentConstructedString(), widthAtIndex + matchWidth) - GetCurrentDrawIndex());
	}
}

void DGUI::TextField::MoveCursorDown()
{
	if(cursorRowIndex < drawingData.size() - 1)
	{
		cursorRowIndex++;

		if(cursorRowIndex >= visibleStringsEnd)
		{
			scrollbar.Scroll(1);

			UpdateVisibleStrings();
		}

		unsigned int widthAtIndex = style->characterSet->GetWidthAtIndex(GetCurrentConstructedString(), GetCurrentDrawIndex());
		SetCursorIndex(style->characterSet->GetIndexAtWidth(GetCurrentConstructedString(), widthAtIndex + matchWidth) - GetCurrentDrawIndex());
	}
}

void DGUI::TextField::MoveCursorLeft()
{
	if(cursorIndex > 0)
		SetCursorIndex(cursorIndex - 1);
	else if(cursorRowIndex > 0)
	{
		int before = cursorRowIndex;

		MoveCursorUp();

		if(before != cursorRowIndex)
			SetCursorIndex(GetCurrentConstructedString().utf8Length);
	}
}

void DGUI::TextField::MoveCursorRight()
{
	if(cursorIndex < GetCurrentDrawCount())
		SetCursorIndex(cursorIndex + 1);
	else if(cursorRowIndex < drawingData.size() - 1)
	{
		int before = cursorRowIndex;

		MoveCursorDown();

		if(before != cursorRowIndex)
			SetCursorIndex(0);
	}
}

void DGUI::TextField::JumpCursorLeft()
{
	MoveCursorLeft();

	const DLib::ConstructedString& constructedString = GetCurrentConstructedString();

	auto iter = constructedString.text.begin();
	utf8::unchecked::advance(iter, cursorIndex + std::get<1>(drawingData[cursorRowIndex]));

	bool nonSpace = false;
	if(cursorIndex < std::get<2>(drawingData[cursorRowIndex]))
	{
		nonSpace = utf8::unchecked::next(iter) != DLib::CharacterSet::SPACE_CHARACTER;
		utf8::unchecked::prior(iter);
	}

	int moveDistance = 0;
	for(int i = cursorIndex + std::get<1>(drawingData[cursorRowIndex]) - 1; i >= 0; i--, moveDistance++)
	{
		if(utf8::unchecked::prior(iter) == DLib::CharacterSet::SPACE_CHARACTER)
		{
			if(nonSpace)
				break;
		}
		else
			nonSpace = true;
	}

	while(moveDistance > cursorIndex)
	{
		moveDistance -= cursorIndex;
		cursorRowIndex--;
		cursorIndex = std::get<2>(drawingData[cursorRowIndex]);
	}

	SetCursorIndex(cursorIndex - moveDistance);
}

void DGUI::TextField::JumpCursorRight()
{
	MoveCursorRight();

	const DLib::ConstructedString& constructedString = GetCurrentConstructedString();

	auto iter = constructedString.text.begin();
	utf8::unchecked::advance(iter, cursorIndex + std::get<1>(drawingData[cursorRowIndex]) - 1);

	bool nonSpace = utf8::unchecked::next(iter) != DLib::CharacterSet::SPACE_CHARACTER;

	int moveDistance = cursorIndex == 0;
    for(unsigned int i = cursorIndex + std::get<1>(drawingData[cursorRowIndex]); i < constructedString.utf8Length; i++, moveDistance++)
	{
		if(utf8::unchecked::next(iter) == DLib::CharacterSet::SPACE_CHARACTER)
		{
			if(nonSpace)
				break;
		}
		else
			nonSpace = true;
	}

	while(moveDistance > std::get<2>(drawingData[cursorRowIndex]) - cursorIndex)
	{
		moveDistance -= std::get<2>(drawingData[cursorRowIndex]) - cursorIndex;
		cursorRowIndex++;
		cursorIndex = 0;
	}

	SetCursorIndex(cursorIndex + moveDistance);
}

void DGUI::TextField::ExtendSelectionToCursor()
{
	if(selectionStringIndex < cursorRowIndex)
	{
		selectionStartIndex = selectionIndex;
		selectionEndIndex = cursorIndex;

		selectionStartStringIndex = selectionStringIndex;
		selectionEndStringIndex = cursorRowIndex;
	}
	else if(selectionStringIndex > cursorRowIndex)
	{
		selectionStartIndex = cursorIndex;
		selectionEndIndex = selectionIndex;

		selectionStartStringIndex = cursorRowIndex;
		selectionEndStringIndex = selectionStringIndex;
	}
	else
	{
		if(cursorIndex > selectionIndex)
		{
			selectionStartIndex = selectionIndex;
			selectionEndIndex = cursorIndex;
		}
		else
		{
			selectionStartIndex = cursorIndex;
			selectionEndIndex = selectionIndex;
		}

		selectionStartStringIndex = cursorRowIndex;
		selectionEndStringIndex = cursorRowIndex;
	}
}

void DGUI::TextField::BeginSelection()
{
	selectionIndex = cursorIndex;
	selectionStringIndex = cursorRowIndex;

	selectionStartIndex = selectionIndex;
	selectionStartStringIndex = selectionStringIndex;
}

void DGUI::TextField::Deselect()
{
	selectionEndIndex = -1;
	selectionStartIndex = -1;

	selectionEndStringIndex = -1;
	selectionStartStringIndex = -1;
}

void DGUI::TextField::EraseSelection()
{
	/*if(SelectionMade())
	{
		style->characterSet->Erase(constructedString, selectionStartIndex, selectionEndIndex - selectionStartIndex);
		SetCursorIndex(selectionStartIndex);
		Deselect();
	}*/
}

bool DGUI::TextField::SelectionMade()
{
	return selectionStartIndex != -1 && selectionEndIndex != -1;
}

void DGUI::TextField::UpdateVisibleStrings()
{
	visibleStringsBegin = scrollbar.GetMinIndex();
	visibleStringsEnd = scrollbar.GetMaxIndex();

	if(cursorRowIndex >= visibleStringsBegin && cursorRowIndex < visibleStringsEnd)
	{
		drawCursor = active;
		cursorPosition.y = area.GetMinPosition().y + (cursorRowIndex - visibleStringsBegin) * style->characterSet->GetLineHeight();
	}
	else
		drawCursor = false;
}

void DGUI::TextField::UpdateCursorPosition()
{

}

const DLib::ConstructedString& DGUI::TextField::GetCurrentConstructedString() const
{
	return constructedStrings[std::get<0>(drawingData[cursorRowIndex])];
}

unsigned int DGUI::TextField::GetCurrentDrawIndex() const
{
	return std::get<1>(drawingData[cursorRowIndex]);
}

unsigned int DGUI::TextField::GetCurrentDrawCount() const
{
	return std::get<2>(drawingData[cursorRowIndex]);
}