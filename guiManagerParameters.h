#ifndef GUIManagerParameters_h__
#define GUIManagerParameters_h__

#include "DLib/ContentParameters.h"

namespace DLib
{
	struct GUIManagerParameters
	{
		GUIManagerParameters();
		~GUIManagerParameters() {};

		//Path to xml file where all styles are located
		std::string stylesPath;
	};
}

#endif // GUIManagerParameters_h__
