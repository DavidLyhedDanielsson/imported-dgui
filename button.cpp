#include "button.h"

DGUI::Button::Button()
	: currentIndex(ButtonStyle::INDICIES::NORMAL)
{

}

DGUI::Button::~Button()
{
}

void DGUI::Button::Init(const DLib::Rect& area, GUIStyle* style, GUIBackground* background, DGUI::GUIStyle* backgroundStyle)
{
	this->style = CastStyleTo<ButtonStyle>(style);

	GUIContainer::Init(area
					   , style
					   , background
					   , nullptr);

	textOffset = this->style->textOffset;
}

void DGUI::Button::Init(const DLib::Rect& area, GUIStyle* style, GUIBackground* background, std::function<void(std::string)> callbackFunction, const std::string& text)
{
	Init(area
		 , style
		 , background
		 , nullptr);

	this->callbackFunction = callbackFunction;
	this->text = this->style->characterSet->ConstructString(text);
}

void DGUI::Button::DrawBackground(DLib::SpriteRenderer* spriteRenderer)
{
	background->Draw(spriteRenderer);
}

void DGUI::Button::DrawMiddle(DLib::SpriteRenderer* spriteRenderer)
{
	spriteRenderer->DrawString(style->characterSet, text, area.GetMinPosition() + textOffset, static_cast<int>(area.GetWidth()), style->textColors[static_cast<int>(currentIndex)]);
}

void DGUI::Button::OnMouseEnter()
{
	if(currentIndex != ButtonStyle::INDICIES::CLICK)
	{
		SetCurrentIndex(ButtonStyle::INDICIES::HOVER);
	}

	SetActive(true);
}

void DGUI::Button::OnMouseExit()
{
	if(currentIndex != ButtonStyle::INDICIES::CLICK)
	{
		SetCurrentIndex(ButtonStyle::INDICIES::NORMAL);
		SetActive(false);
	}
}

void DGUI::Button::OnMouseDown(const DLib::KeyState& keyState, const glm::vec2& mousePosition)
{
	if(keyState.key == GLFW_MOUSE_BUTTON_LEFT && currentIndex == ButtonStyle::INDICIES::HOVER)
	{
		SetCurrentIndex(ButtonStyle::INDICIES::CLICK);
	}
}

void DGUI::Button::OnMouseUp(const DLib::KeyState& keyState, const glm::vec2& mousePosition)
{
	if(area.Contains(mousePosition))
	{
		if(currentIndex == ButtonStyle::INDICIES::CLICK)
		{
			SetCurrentIndex(ButtonStyle::INDICIES::HOVER);


			if(callbackFunction != nullptr)
				callbackFunction(text.text);
		}
		else
			SetCurrentIndex(ButtonStyle::INDICIES::HOVER);
	}
	else
	{
		SetCurrentIndex(ButtonStyle::INDICIES::NORMAL);
		SetActive(false);
	}
}

void DGUI::Button::SetCallbackFunction(std::function<void(std::string)> callbackFunction)
{
	this->callbackFunction = callbackFunction;
}

void DGUI::Button::AlignText(int alignment, bool includeStyleOffset)
{
	switch(alignment & static_cast<int>(DIRECTIONS::Y_BITS))
	{
		case TOP:
			textOffset.y = 0.0f;
			break;
		case BOTTOM:
			textOffset.y = area.GetHeight() - style->characterSet->GetLineHeight();
			break;
		case CENTER_Y:
			textOffset.y = std::round(area.GetHeight() * 0.5f - style->characterSet->GetLineHeight() * 0.5f);
			break;
		default:
			DLib::Logger::LogLine(DLib::LOG_TYPE_WARNING, "Invalid button alignment: \"" + std::to_string(alignment & DIRECTIONS::Y_BITS) + "\"");
			break;
	}
	switch(alignment & static_cast<int>(~DIRECTIONS::Y_BITS))
	{
		case LEFT:
			textOffset.x = 0.0f;
			break;
		case RIGHT:
			textOffset.x = area.GetWidth() - text.width;
			break;
		case CENTER_X:
			textOffset.x = std::round(area.GetWidth() * 0.5f - text.width * 0.5f);
			break;
		default:
			DLib::Logger::LogLine(DLib::LOG_TYPE_WARNING, "Invalid button alignment: \"" + std::to_string(alignment & (~DIRECTIONS::Y_BITS)) + "\"");
			break;
	}

	if(includeStyleOffset)
		textOffset += style->textOffset;
}

void DGUI::Button::SetCurrentIndex(ButtonStyle::INDICIES index)
{
	currentIndex = index;
	background->ChangePreset(static_cast<int>(currentIndex));
}
