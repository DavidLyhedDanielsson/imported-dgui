#ifndef OPENGLWINDOW_BACKGROUND_H
#define OPENGLWINDOW_BACKGROUND_H

#include "guiStyle.h"

#include <dlib/spriteRenderer.h>

namespace DGUI
{
	class GUIBackground
	{
	public:
		GUIBackground();
		virtual ~GUIBackground();

		virtual void Init(GUIStyle* style, const DLib::Rect& area) = 0;
		virtual void Draw(DLib::SpriteRenderer* spriteRenderer) = 0;

		virtual void AreaChanged(const DLib::Rect& area);
		virtual void ChangePreset(int preset);

		virtual GUIBackground* Clone() = 0;
	protected:
		DLib::Rect area;

		int preset;

		//************************************
		// Method:		CastStyleTo
		// Parameter:	GUIStyle* style
		// Returns:		T* - the casted object (dynamic_cast)
		// Description: Tries to cast the given style to the given type, if the cast fails an error message is logged
		//************************************
		template<typename T>
		T* CastStyleTo(GUIStyle* style)
		{
			T* castStyle = dynamic_cast<T*>(style);

			if(castStyle == nullptr)
				DLib::Logger::LogLine(DLib::LOG_TYPE_WARNING, "Couldn't cast style to " + std::string(typeid(T).name()));

			return castStyle;
		}
	};
}

#endif //OPENGLWINDOW_BACKGROUND_H
