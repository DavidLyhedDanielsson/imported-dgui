#include "guiContainer.h"

DGUI::GUIContainer::GUIContainer()
	: active(true)
	, draw(true)
	, mouseInside(false)
{
}

DGUI::GUIContainer::~GUIContainer()
{
}

void DGUI::GUIContainer::Init(const DLib::Rect& area, GUIStyle* style, GUIBackground* background, DGUI::GUIStyle* backgroundStyle)
{
	this->area = area;
	this->background = background;

	if(this->background != nullptr)
		this->background->Init(backgroundStyle, this->area);
}

void DGUI::GUIContainer::Draw(DLib::SpriteRenderer* spriteRenderer)
{
	DrawBackground(spriteRenderer);
	DrawMiddle(spriteRenderer);
	DrawForeground(spriteRenderer);
}

//////////////////////////////////////////////////////////////////////////
//SETTERS
//////////////////////////////////////////////////////////////////////////

void DGUI::GUIContainer::SetPosition(const glm::vec2& newPosition)
{
	area.SetPos(newPosition.x, newPosition.y);
	background->AreaChanged(area);

}

void DGUI::GUIContainer::SetPosition(float x, float y)
{
	area.SetPos(x, y);
	background->AreaChanged(area);
}

void DGUI::GUIContainer::SetSize(const glm::vec2& newSize)
{
	area.SetSize(newSize.x, newSize.y);
	background->AreaChanged(area);
}

void DGUI::GUIContainer::SetSize(float x, float y)
{
	area.SetSize(x, y);
	background->AreaChanged(area);
}

void DGUI::GUIContainer::SetArea(const DLib::Rect& newArea)
{
	area = newArea;
	background->AreaChanged(area);
}

DLib::Rect DGUI::GUIContainer::GetArea() const
{
	return area;
}

glm::vec2 DGUI::GUIContainer::GetPosition() const
{
	return area.GetMinPosition();
}

glm::vec2 DGUI::GUIContainer::GetSize() const
{
	return area.GetSize();
}

bool DGUI::GUIContainer::GetDraw() const
{
	return draw;
}

void DGUI::GUIContainer::SetDraw(bool draw)
{
	this->draw = draw;
}

bool DGUI::GUIContainer::GetActive() const
{
	return active;
}

void DGUI::GUIContainer::SetActive(bool active)
{
	this->active = active;
}
