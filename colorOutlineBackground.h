#ifndef OPENGLWINDOW_COLOROUTLINEBACKGROUND_H
#define OPENGLWINDOW_COLOROUTLINEBACKGROUND_H

#include "guiBackground.h"

#include "colorOutlineBackgroundStyle.h"

namespace DGUI
{
	class ColorOutlineBackground
			: public GUIBackground
	{
	public:
		ColorOutlineBackground();
		~ColorOutlineBackground();

		virtual void Init(GUIStyle* style, const DLib::Rect& area) override;
		virtual void Draw(DLib::SpriteRenderer* spriteRenderer) override;
		virtual void AreaChanged(const DLib::Rect& area) override;

	protected:
		DLib::Rect outlineRect;

		ColorOutlineBackgroundStyle* style;

		void UpdateOutlineRect();
	};
}

#endif //OPENGLWINDOW_COLOROUTLINEBACKGROUND_H
