#include "scrollbar.h"

#include <dlib/input.h>

DGUI::Scrollbar::Scrollbar()
	: mouseDown(false)
	, lockedToBottom(false)
	, beginIndex(0)
	, endIndex(0)
	, visibleItems(0)
	, maxItems(0)
	, grabY(0)
{
}

DGUI::Scrollbar::~Scrollbar()
{
}

void DGUI::Scrollbar::Init(const DLib::Rect& area, GUIStyle* style, GUIBackground* background, DGUI::GUIStyle* backgroundStyle)
{
	this->style = CastStyleTo<ScrollbarStyle>(style);

	DLib::Rect actualArea = area;
	actualArea.SetSize(this->style->thumbWidth, area.GetHeight());
	actualArea.SetPos(area.GetMaxPosition().x - this->style->thumbWidth, area.GetMinPosition().y);

	GUIContainer::Init(actualArea, style, background, backgroundStyle);

	UpdateThumbPosition();
	UpdateThumbSize();

	beginIndex = 0;
	endIndex = 0;
}

void DGUI::Scrollbar::Update(std::chrono::nanoseconds delta)
{
	if(mouseDown)
	{
		if(grabY != -1
			&& DLib::Input::MouseMoved())
		{
			thumb.SetPos(thumb.GetMinPosition().x, DLib::Input::GetMousePosition().y - grabY);
			ClampThumbPosition();
			UpdateThumbIndex();
		}
	}
}

void DGUI::Scrollbar::DrawBackground(DLib::SpriteRenderer* spriteRenderer)
{
	background->Draw(spriteRenderer);
	//spriteRenderer->Draw(area, style->backgroundColor);
}

void DGUI::Scrollbar::DrawMiddle(DLib::SpriteRenderer* spriteRenderer)
{
	spriteRenderer->Draw(thumb, style->thumbColor);
}

void DGUI::Scrollbar::OnMouseDown(const DLib::KeyState& keyState, const glm::vec2& mousePosition)
{
	mouseDown = true;
	lockedToBottom = false;

	if(thumb.Contains(mousePosition))
		grabY = mousePosition.y - thumb.GetMinPosition().y;
	else
		grabY = -1;
}

void DGUI::Scrollbar::OnMouseUp(const DLib::KeyState& keyState, const glm::vec2& mousePosition)
{
	mouseDown = false;
	grabY = -1;

	if(endIndex == maxItems)
	{
		lockedToBottom = true;

		beginIndex = maxItems - visibleItems;
		if(beginIndex < 0)
			beginIndex = 0;
	}
	else
		lockedToBottom = false;
}

void DGUI::Scrollbar::OnScroll(double x, double y)
{
	Scroll(static_cast<int>(-y));
}

void DGUI::Scrollbar::Scroll(int index)
{
	beginIndex += index;
	endIndex += index;

	if(beginIndex < 0)
	{
		beginIndex = 0;
		endIndex = beginIndex + std::min(visibleItems, maxItems);
	}
	else if(endIndex > maxItems)
	{
		endIndex = maxItems;
		beginIndex = endIndex - std::min(visibleItems, maxItems);
	}

	lockedToBottom = endIndex == maxItems;

	UpdateThumbPosition();
}

void DGUI::Scrollbar::ScrollTo(int index)
{
	if(index > beginIndex && index < endIndex)
		return;

	int lowDist = index - beginIndex;
	int highDist = index - endIndex;

	if(abs(highDist) < abs(lowDist))
	{
		//Scroll down
		endIndex = index + 1;
		beginIndex = std::max(0, endIndex - visibleItems);
	}
	else
	{
		//Scroll up
		beginIndex = index;
		endIndex = std::min(maxItems, beginIndex + visibleItems);
	}

	UpdateThumbPosition();
}

void DGUI::Scrollbar::SetVisibleItems(int visibleItems)
{
	int delta = visibleItems - this->visibleItems;
	this->visibleItems = visibleItems;

	if(endIndex - beginIndex + delta < visibleItems
		|| delta == 0)
		return;

	if(lockedToBottom
		&& delta < 1)
	{
		//Subtract from beginIndex instead to preserve endIndex
		beginIndex += -delta;
		if(beginIndex < 0)
			beginIndex = 0;

		if(endIndex - beginIndex > visibleItems)
			endIndex = beginIndex + visibleItems;
	}
	else
	{
		endIndex += delta;
		if(endIndex > maxItems)
			endIndex = maxItems;

		if(endIndex - beginIndex < visibleItems)
		{
			beginIndex = endIndex - visibleItems;

			if(beginIndex < 0)
				beginIndex = 0;
		}
	}

	UpdateThumbSize();
	UpdateThumbPosition();
}

void DGUI::Scrollbar::SetMaxItems(int maxItems)
{
	int delta = this->maxItems - maxItems;
	this->maxItems = maxItems;

	UpdateThumbSize();

	if(lockedToBottom)
	{
		endIndex = maxItems;
		beginIndex = std::max(endIndex - visibleItems, 0);
	}
	else
	{
		endIndex = beginIndex + visibleItems;
		if(endIndex > maxItems)
		{
			endIndex = maxItems;
			beginIndex = std::max(endIndex - visibleItems, 0);
		}

		//TODO: lockToBottom = true?
	}

	//ClampIndex();
	UpdateThumbPosition();

	/*if(maxItems > visibleItems)
	{
	if(lockedToBottom)
	{
	beginIndex = maxItems - visibleItems;
	if(beginIndex < 0)
	beginIndex = 0;

	endIndex = maxItems;
	}
	else
	{
	if(endIndex - beginIndex < visibleItems)
	endIndex++;
	}

	UpdateThumbPosition();
	}
	else
	{
	beginIndex = 0;
	endIndex = maxItems;
	}*/
}

void DGUI::Scrollbar::UpdateThumbSize()
{
	if(maxItems > visibleItems)
	{
		float newHeight = area.GetHeight() * (visibleItems / static_cast<float>(maxItems));
		if(newHeight < style->thumbMinSize)
			newHeight = style->thumbMinSize;

		thumb.SetSize(style->thumbWidth, newHeight);
	}
	else
	{
		thumb = area;
	}
}

void DGUI::Scrollbar::UpdateThumbIndex()
{
	int effectiveHeight = GetEffectiveHeight();

	float beginPerc = 0.0f;
	if(effectiveHeight != 0)
		beginPerc = (thumb.GetMinPosition().y - area.GetMinPosition().y) / GetEffectiveHeight();

	beginIndex = round((maxItems - visibleItems) * beginPerc);
	endIndex = beginIndex + visibleItems;

	if(endIndex > maxItems)
		endIndex = maxItems;
}

void DGUI::Scrollbar::UpdateThumbPosition()
{
	if(maxItems == 0)
	{
		thumb = area; Thumb is broken?
		return;
	}

	if(!lockedToBottom)
	{
		if(maxItems == visibleItems)
			thumb = area;
		else
		{
			float beginperc = static_cast<float>(beginIndex) / (maxItems - visibleItems);

			thumb.SetPos(area.GetMaxPosition().x - style->thumbWidth, area.GetMinPosition().y + GetEffectiveHeight() * beginperc);
			ClampThumbPosition();
		}
	}
	else
	{
		//Having logic for this avoids cases where beginPerc is 0.999something
		thumb.SetPos(area.GetMaxPosition().x - style->thumbWidth, area.GetMaxPosition().y - thumb.GetHeight());
	}
}

int DGUI::Scrollbar::GetVisibleItems() const
{
	return visibleItems;
}

int DGUI::Scrollbar::GetMaxItems() const
{
	return maxItems;
}

void DGUI::Scrollbar::ClampThumbPosition()
{
	int effectiveHeight = GetEffectiveHeight();

	if(thumb.GetMinPosition().y < area.GetMinPosition().y)
		thumb.SetPos(thumb.GetMinPosition().x, area.GetMinPosition().y);
	else
	{
		int effectiveHeight = GetEffectiveHeight();
		float relative = GetThumbRelativeY();

		if(relative > effectiveHeight)
			thumb.SetPos(thumb.GetMinPosition().x, area.GetMaxPosition().y - thumb.GetHeight());
	}
}

int DGUI::Scrollbar::GetMinIndex() const
{
	return beginIndex;
}

int DGUI::Scrollbar::GetMaxIndex() const
{
	return endIndex;
}

int DGUI::Scrollbar::GetEffectiveHeight()
{
	return static_cast<int>(area.GetHeight() - thumb.GetHeight());
}

float DGUI::Scrollbar::GetThumbRelativeY()
{
	return thumb.GetMinPosition().y - area.GetMinPosition().y;
}

void DGUI::Scrollbar::SetPosition(const glm::vec2& newPosition)
{
	SetPosition(newPosition.x, newPosition.y);
}

void DGUI::Scrollbar::SetPosition(float x, float y)
{
	GUIContainer::SetPosition(x - style->thumbWidth, y);

	UpdateThumbPosition();
}

void DGUI::Scrollbar::SetSize(const glm::vec2& newSize)
{
	SetSize(newSize.x, newSize.y);
}

void DGUI::Scrollbar::SetSize(float x, float y)
{
	area.SetSize(x, y);

	UpdateThumbSize();
	UpdateThumbPosition();
}

void DGUI::Scrollbar::SetArea(const DLib::Rect& newArea)
{
	SetPosition(newArea.GetMinPosition());
	SetSize(newArea.GetSize());
}

bool DGUI::Scrollbar::Contains(const glm::vec2& position) const
{
	return area.Contains(position);
}

