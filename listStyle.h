#ifndef OPENGLWINDOW_LISTSTYLE_H
#define OPENGLWINDOW_LISTSTYLE_H

#include "guiStyle.h"

#include "scrollbarStyle.h"

#include <glm/vec4.hpp>

namespace DGUI
{
	struct ListStyle
			: public GUIStyle
	{
		ListStyle()
				: scrollbarStyle(nullptr)
				, scrollbarBackgroundStyle(nullptr)
				, scrollbarBackground(nullptr)
		{ }
		~ListStyle()
		{ }

		ScrollbarStyle* scrollbarStyle;
		DGUI::GUIStyle* scrollbarBackgroundStyle;

		GUIBackground* scrollbarBackground;
	};
}

#endif //OPENGLWINDOW_LISTSTYLE_H
