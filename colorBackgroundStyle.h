#ifndef OPENGLWINDOW_COLORBACKGROUNDSTYLE_H
#define OPENGLWINDOW_COLORBACKGROUNDSTYLE_H

#include "guiStyle.h"

#include <vector>

#include <glm/vec4.hpp>

namespace DGUI
{
	struct ColorBackgroundStyle
			: public GUIStyle
	{
	public:
		ColorBackgroundStyle();
		~ColorBackgroundStyle();

		std::vector<glm::vec4> colors;
	};
}


#endif //OPENGLWINDOW_COLORBACKGROUNDSTYLE_H
