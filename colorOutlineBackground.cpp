#include "colorOutlineBackground.h"

DGUI::ColorOutlineBackground::ColorOutlineBackground()
{

}

DGUI::ColorOutlineBackground::~ColorOutlineBackground()
{

}

void DGUI::ColorOutlineBackground::Init(GUIStyle* style, const DLib::Rect& area)
{
	this->style = CastStyleTo<ColorOutlineBackgroundStyle>(style);
	this->area = area;

	UpdateOutlineRect();
}

void DGUI::ColorOutlineBackground::Draw(DLib::SpriteRenderer* spriteRenderer)
{
	spriteRenderer->Draw(outlineRect, style->outlineColors[preset]);
	spriteRenderer->Draw(area, style->backgroundColors[preset]);
}

void DGUI::ColorOutlineBackground::AreaChanged(const DLib::Rect& area)
{
	GUIBackground::AreaChanged(area);
	UpdateOutlineRect();
}

void DGUI::ColorOutlineBackground::UpdateOutlineRect()
{
	outlineRect = area;

	glm::vec2 positionModifier;
	glm::vec2 sizeModifier;

	//Update positionModifier and sizeModifier depending on style exclusions
	if((style->excludeSides & DGUI::DIRECTIONS::Y_BITS & DGUI::DIRECTIONS::TOP) != DGUI::DIRECTIONS::TOP)
	{
		positionModifier.y -= style->outlineThickness;
		sizeModifier.y += style->outlineThickness;
	}
	if((style->excludeSides & DGUI::DIRECTIONS::Y_BITS & DGUI::DIRECTIONS::BOTTOM) != DGUI::DIRECTIONS::BOTTOM)
	{
		sizeModifier.y += style->outlineThickness;
	}
	if((style->excludeSides & DGUI::DIRECTIONS::X_BITS & DGUI::DIRECTIONS::LEFT) != DGUI::DIRECTIONS::LEFT)
	{
		positionModifier.x -= style->outlineThickness;
		sizeModifier.x += style->outlineThickness;
	}
	if((style->excludeSides & DGUI::DIRECTIONS::X_BITS & DGUI::DIRECTIONS::RIGHT) != DGUI::DIRECTIONS::RIGHT)
	{
		sizeModifier.x += style->outlineThickness;
	}

	outlineRect += positionModifier;
	outlineRect.SetSize(outlineRect.GetSize() + sizeModifier);
}
