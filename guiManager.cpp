#include "guiManager.h"

#include <dlib/input.h>
#include <dlib/logger.h>

#include "textBoxStyle.h"

DGUI::GUIManager::GUIManager()
{
}

DGUI::GUIManager::~GUIManager()
{
}

void DGUI::GUIManager::AddContainer(GUIContainer* container)
{
	containers.push_back(container);
}

void DGUI::GUIManager::SetContainers(const std::vector<DGUI::GUIContainer*>& containers)
{
	this->containers = containers;
}

DGUI::GUIBackground* DGUI::GUIManager::AddBackground(DGUI::GUIBackground* background)
{
	backgrounds.emplace_back(background);

	return background;
}

void DGUI::GUIManager::AddStyle(DGUI::GUIStyle* style)
{
	styles.emplace_back(style);
}

void DGUI::GUIManager::Update(std::chrono::nanoseconds delta)
{
	glm::vec2 mousePosition = DLib::Input::GetMousePosition();
	for(GUIContainer* container : containers)
	{
		if(container->area.Contains(mousePosition))
		{
			if(!container->mouseInside)
			{
				container->OnMouseEnter();
				container->mouseInside = true;
			}
		}
		else
		{
			if(container->mouseInside)
			{
				container->OnMouseExit();
				container->mouseInside = false;
			}
		}

		container->Update(delta);
	}
}

void DGUI::GUIManager::Draw(DLib::SpriteRenderer* spriteRenderer)
{
	for(GUIContainer* container : containers)
		if(container->draw)
			container->Draw(spriteRenderer);
}

void DGUI::GUIManager::KeyEvent(const DLib::KeyState& keyState)
{
	if(keyState.action > 0)
	{
		for(GUIContainer* container : containers)
			if(container->active)
				container->OnKeyDown(keyState);
	}
	else
	{
		for(GUIContainer* container : containers)
			if(container->active)
				container->OnKeyUp(keyState);
	}
}

void DGUI::GUIManager::MouseEvent(const DLib::KeyState& keyState)
{
	glm::vec2 mousePosition = DLib::Input::GetMousePosition();

	if(keyState.action > 0)
	{
		for(GUIContainer* container : containers)
			if(container->active)
				container->OnMouseDown(keyState, mousePosition);
	}
	else
	{
		for(GUIContainer* container : containers)
			if(container->active)
				container->OnMouseUp(keyState, mousePosition);
	}
}

void DGUI::GUIManager::CharEvent(unsigned int keyCode)
{
	for(GUIContainer* container : containers)
		if(container->active)
			container->OnChar(keyCode);
}

void DGUI::GUIManager::ScrollEvent(double x, double y)
{
	for(GUIContainer* container : containers)
		if(container->active)
			container->OnScroll(x, y);
}

void DGUI::GUIManager::Clear()
{
	ClearContainers();
	ClearBackgrounds();
	ClearStyles();
}

void DGUI::GUIManager::ClearContainers()
{
	for(GUIContainer* container : containers)
		delete container;

	containers.clear();
}

void DGUI::GUIManager::ClearBackgrounds()
{
	for(GUIBackground* background : backgrounds)
		delete background;

	backgrounds.clear();
}

void DGUI::GUIManager::ClearStyles()
{
	for(GUIStyle* style : styles)
		delete style;

	styles.clear();
}
