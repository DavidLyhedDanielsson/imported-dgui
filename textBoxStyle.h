#ifndef TextBoxStyle_h__
#define TextBoxStyle_h__

#include "guiStyle.h"

#include <dlib/rect.h>

#include <glm/vec2.hpp>
#include <glm/vec4.hpp>

namespace DGUI
{
	struct TextBoxStyle : public GUIStyle
	{
		TextBoxStyle() 
			: textHightlightCOlor(173.0f / 255.0f, 214.0f / 255.0f, 1.0f, 1.0f)
			, textColorNormal(0.0f, 0.0f, 0.0f, 1.0f)
			, textColorSelected(0.0f, 0.0f, 0.0f, 1.0f)
			, cursorSize(0.0f, 0.0f)
			, cursorColorNormal(0.0f, 0.0f, 0.0f, 1.0f)
			, cursorColorBlink(0.0f, 0.0f, 0.0f, 0.0f)
			, cursorOffset(0.0f, 0.0f)
			, characterSet(nullptr)
		{};
		virtual ~TextBoxStyle() {};

		glm::vec4 textHightlightCOlor;
		glm::vec4 textColorNormal;
		glm::vec4 textColorSelected;

		glm::vec2 cursorSize;
		glm::vec4 cursorColorNormal;
		glm::vec4 cursorColorBlink;
		glm::vec2 cursorOffset;

		const DLib::CharacterSet* characterSet;
	};
}

#endif // TextBoxStyle_h__
