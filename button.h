#ifndef Button_h__
#define Button_h__

#include "guiContainer.h"

#include <functional>

#include "buttonStyle.h"
#include "common.h"
#include "guiBackground.h"

#include <glm/vec4.hpp>

namespace DGUI
{
	class Button :
			public GUIContainer
	{
	public:
		Button();
		~Button();

		virtual void Init(const DLib::Rect& area, GUIStyle* style, GUIBackground* background, DGUI::GUIStyle* backgroundStyle) override;
		virtual void Init(const DLib::Rect& area, GUIStyle* style, GUIBackground* background, std::function<void(
				std::string)> callbackFunction, const std::string& text);

		virtual void DrawBackground(DLib::SpriteRenderer* spriteRenderer) override;
		virtual void DrawMiddle(DLib::SpriteRenderer* spriteRenderer) override;

		virtual void OnMouseEnter() override;
		virtual void OnMouseExit() override;
		virtual void OnMouseDown(const DLib::KeyState& keyState, const glm::vec2& mousePosition) override;
		virtual void OnMouseUp(const DLib::KeyState& keyState, const glm::vec2& mousePosition) override;

		void SetCallbackFunction(std::function<void(std::string)> callbackFunction);

		//************************************
		// Method:		AlignText
		// Returns:		void
		// Parameter:	int alignment - what to align to. Use the DLib::DIRECTIONS enum
		// Parameter:	bool includeStyleOffset - offset the text by the style's text offset after aligning??
		// Description:
		//************************************
		virtual void AlignText(int alignment, bool includeStyleOffset);

	protected:
		DGUI::ButtonStyle* style;

		std::function<void(std::string)> callbackFunction;

		ButtonStyle::INDICIES currentIndex;

		DLib::ConstructedString text;

		glm::vec2 textOffset;

		void SetCurrentIndex(ButtonStyle::INDICIES index);
	};
}

#endif // Button_h__
