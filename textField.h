#ifndef TextField_h__
#define TextField_h__

#include "guiContainer.h"

#include "textFieldStyle.h"

#include <dlib/characterSet.h>
#include <dlib/constructedString.h>
#include <dlib/timer.h>

#include "textBoxStyle.h"
#include "scrollbar.h"
#include "scrollbarStyle.h"

namespace DGUI
{
	class TextField :
		public GUIContainer
	{
	public:
		TextField();
		virtual ~TextField();

		virtual void Init(const DLib::Rect& area, GUIStyle* style, GUIBackground* background, DGUI::GUIStyle* backgroundStyle) override;
		virtual void Update(std::chrono::nanoseconds delta) override;
		//virtual void Draw(DLib::SpriteRenderer* spriteRenderer) override;

		virtual void DrawBackground(DLib::SpriteRenderer* spriteRenderer) override;
		virtual void DrawMiddle(DLib::SpriteRenderer* spriteRenderer) override;
		virtual void DrawForeground(DLib::SpriteRenderer* spriteRenderer) override;

		void AddText(const std::string& text);
		std::vector<DLib::ConstructedString> GetText();
		std::string GetText(int stringIndex);
		std::string GetSelectedText();

		void SetCharacterSet(const std::string& fontPath, DLib::ContentManager* contentManager);
		virtual void SetCharacterSet(const DLib::CharacterSet* characterSet);
		void SetCutoffString(const std::string& string); //TODO: USE CUTOFF STRING?

		bool allowEdit;

		virtual void SetPosition(const glm::vec2& newPosition) override;
		virtual void SetPosition(float x, float y) override;
		virtual void SetSize(const glm::vec2& newSize) override;
		virtual void SetSize(float x, float y) override;

		virtual void OnKeyDown(const DLib::KeyState& keyState) override;
		virtual void OnMouseEnter() override;
		virtual void OnMouseExit() override;
		virtual void OnMouseDown(const DLib::KeyState& keyState, const glm::vec2& mousePosition) override;
		virtual void OnMouseUp(const DLib::KeyState& keyState, const glm::vec2& mousePosition) override;
		virtual void OnScroll(double x, double y) override;

	protected:
		//const DLib::CharacterSet* characterSet;
		std::vector<DLib::ConstructedString> constructedStrings;
		DLib::ConstructedString constructedCutoffString;

		glm::vec4 textColor;
		glm::vec4 hightlightColor;

		TextFieldStyle* style;

		//Used as a general flag. If cursor is drawn then input should be accepted and processed
		bool drawCursor;
		bool mouseDown;

		Scrollbar scrollbar;

		//[line]<constructedStringsIndex, drawStartIndex, drawCount>
		std::vector<std::tuple<int, int, int>> drawingData;

		//////////////////////////////////////////////////////////////////////////
		//CURSOR
		//////////////////////////////////////////////////////////////////////////
		glm::vec4 cursorColor;

		int cursorIndex;
		int cursorRowIndex;

		int selectionIndex;
		int selectionStartIndex;
		int selectionEndIndex;

		int selectionStringIndex;
		int selectionStartStringIndex;
		int selectionEndStringIndex;

		int visibleStringsBegin;
		int visibleStringsEnd;
		int visibleStringsMax;

		int matchWidth;

		DLib::Timer cursorBlinkTimer;
		const std::chrono::nanoseconds cursorBlinkTime = std::chrono::milliseconds(750); //Nanoseconds

		glm::vec2 cursorPosition;

		//<width, index>
		unsigned int GetCursorIndex(int width, int line);

		void UpPressed(const DLib::KeyState& keyState);
		void DownPressed(const DLib::KeyState& keyState);
		void LeftPressed(const DLib::KeyState& keyState);
		void RightPressed(const DLib::KeyState& keyState);
		void BackspacePressed(const DLib::KeyState& keyState);
		void DeletePressed(const DLib::KeyState& keyState);
		void HomePressed(const DLib::KeyState& keyState);
		void EndPressed(const DLib::KeyState& keyState);

		void SetCursorIndex(unsigned int newIndex);
		void SetRowIndex(unsigned int newIndex);
		void MoveCursorUp();
		void MoveCursorDown();
		void MoveCursorLeft();
		void MoveCursorRight();
		void JumpCursorLeft();
		void JumpCursorRight();

		void ExtendSelectionToCursor();
		void BeginSelection();
		void Deselect();
		void EraseSelection();
		bool SelectionMade();

		void UpdateText();
		void UpdateVisibleStrings();
		void UpdateCursorPosition();

		const DLib::ConstructedString& GetCurrentConstructedString() const;
        unsigned int GetCurrentDrawIndex() const;
        unsigned int GetCurrentDrawCount() const;
	};
}

#endif // TextField_h__
