#ifndef GUIManager_h__
#define GUIManager_h__

#include <vector>

#include <dlib/spriteRenderer.h>
#include <dlib/xmlFile.h>

#include "guiContainer.h"

namespace DGUI
{
	class GUIManager //TODO: cache active/inactive containers?
	{
	public:
		GUIManager();
		~GUIManager();

		void AddContainer(GUIContainer* container);
		void SetContainers(const std::vector<GUIContainer*>& containers);

		DGUI::GUIBackground* AddBackground(GUIBackground* background);
		void AddStyle(GUIStyle* style);

		void KeyEvent(const DLib::KeyState& keyState);
		void MouseEvent(const DLib::KeyState& keyState);
		void CharEvent(unsigned int keyCode);
		void ScrollEvent(double x, double y);

		void Update(std::chrono::nanoseconds delta);
		void Draw(DLib::SpriteRenderer* spriteRenderer); //TODO: Use DrawBackground etc.

		void Clear();
		void ClearContainers();
		void ClearBackgrounds();
		void ClearStyles();

		template<typename T>
		T* GenerateBackground()
		{
			T* newBackground = new T();
			AddBackground(newBackground);
			return newBackground;
		}

		template<typename T>
		T* GenerateStyle()
		{
			T* newStyle = new T();
			AddStyle(newStyle);
			return newStyle;
		}

	private:
		std::vector<GUIContainer*> containers;
		std::vector<GUIBackground*> backgrounds;
		std::vector<GUIStyle*> styles;
	};
}

#endif // GUIManager_h__
