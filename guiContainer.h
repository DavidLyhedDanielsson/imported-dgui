#ifndef GUIContainer_h__
#define GUIContainer_h__

#include <chrono>

#include <glm/vec2.hpp>

#include <dlib/rect.h>
#include <dlib/spriteRenderer.h>
#include <dlib/keyState.h>

#include "guiStyle.h"
#include "guiBackground.h"

namespace DGUI
{
	class GUIContainer
	{
		friend class GUIManager;
	public:
		GUIContainer();
		virtual ~GUIContainer();

		//************************************
		// Method:		Init
		// FullName:	DGUI::GUIContainer::Init
		// Access:		public 
		// Returns:		void
		// Qualifier:	
		// Parameter:	float x - position
		// Parameter:	float y - position
		// Parameter:	float width
		// Parameter:	float height
		// Description:	Sets the area of this object
		//************************************
		virtual void Init(const DLib::Rect& area, DGUI::GUIStyle* style, DGUI::GUIBackground* background, DGUI::GUIStyle* backgroundStyle);

		virtual void Update(std::chrono::nanoseconds delta) {};
		virtual void Draw(DLib::SpriteRenderer* spriteRenderer);

		virtual void DrawBackground(DLib::SpriteRenderer* spriteRenderer) {}
		virtual void DrawMiddle(DLib::SpriteRenderer* spriteRenderer) {}
		virtual void DrawForeground(DLib::SpriteRenderer* spriteRenderer) {}

		virtual void SetPosition(const glm::vec2& newPosition);
		virtual void SetPosition(float x, float y);
		virtual void SetSize(const glm::vec2& newSize);
		virtual void SetSize(float x, float y);
		//************************************
		// Method:		SetArea
		// FullName:	DGUI::GUIContainer::SetArea
		// Access:		virtual public 
		// Returns:		void
		// Qualifier:	
		// Parameter:	const DLib::Rect& newArea
		// Description:	Sets the area of this object. This is SetPosition() and SetSize() combined
		//************************************
		virtual void SetArea(const DLib::Rect& newArea);

		virtual DLib::Rect GetArea() const;
		virtual glm::vec2 GetPosition() const;
		virtual glm::vec2 GetSize() const;

		virtual bool GetDraw() const;
		virtual void SetDraw(bool draw);

		virtual bool GetActive() const;
		virtual void SetActive(bool active);

		//************************************
		// Method:		OnMouseEnter
		// FullName:	DGUI::GUIContainer::OnMouseEnter
		// Access:		virtual protected
		// Returns:		void
		// Qualifier:
		// Description:	Called whenever the cursor enters "area". Should be protected
		//************************************
		virtual void OnMouseEnter() {};
		//************************************
		// Method:		OnMouseExit
		// FullName:	DGUI::GUIContainer::OnMouseExit
		// Access:		virtual protected
		// Returns:		void
		// Qualifier:
		// Description:	Called whenever the cursor exits "area". Should be protected
		//************************************
		virtual void OnMouseExit() {};
		//************************************
		// Method:		OnMouseDown
		// FullName:	DGUI::GUIContainer::OnMouseDown
		// Access:		virtual protected
		// Returns:		void
		// Qualifier:
		// Parameter:	DLib::KeyState keyState
		// Description:	Called whenever a mouse button is pressed and "active" is true
		//************************************
		virtual void OnMouseDown(const DLib::KeyState& keyState, const glm::vec2& mousePosition) {};
		//************************************
		// Method:		OnMouseUp
		// FullName:	DGUI::GUIContainer::OnMouseUp
		// Access:		virtual protected
		// Returns:		void
		// Qualifier:
		// Parameter:	DLib::KeyState keyState
		// Description:	Called whenever a mouse button is released and "active" is true
		//************************************
		virtual void OnMouseUp(const DLib::KeyState& keyState, const glm::vec2& mousePosition) {};
		//************************************
		// Method:		OnKeyDown
		// FullName:	DGUI::GUIContainer::OnKeyDown
		// Access:		virtual protected
		// Returns:		void
		// Qualifier:
		// Parameter:	DLib::KeyState keyState
		// Description:	Called whenever a button is pressed and "active" is true
		//************************************
		virtual void OnKeyDown(const DLib::KeyState& keyState) {};
		//************************************
		// Method:		OnKeyUp
		// FullName:	DGUI::GUIContainer::OnKeyUp
		// Access:		virtual protected
		// Returns:		void
		// Qualifier:
		// Parameter:	DLib::KeyState keyState
		// Description:	Called whenever a button is released and "active" is true
		//************************************
		virtual void OnKeyUp(const DLib::KeyState& keyState) {};
		//************************************
		// Method:		OnChar
		// FullName:	DGUI::GUIContainer::OnChar
		// Access:		virtual protected
		// Returns:		void
		// Qualifier:
		// Parameter:	DLib::KeyState keyState
		// Description:	Called whenever the user presses a button to enter text and "active" is true
		//************************************
		virtual void OnChar(unsigned int keyCode) {};
		//************************************
		// Method:		OnScroll
		// FullName:	DGUI::GUIContainer::OnScroll
		// Access:		virtual protected
		// Returns:		void
		// Qualifier:
		// Parameter:	double x
		// Parameter:	double y
		// Description:	Called whenever the user scrolls and "active" is true
		//************************************
		virtual void OnScroll(double x, double y) {};

	protected:
		//If "active" is true this object will recieve mouse/key events
		bool active;
		//Should this object be drawn?
		bool draw;

		GUIBackground* background;

		//Essentially the hitbox of this object
		DLib::Rect area;

		//************************************
		// Method:		CastStyleTo
		// Parameter:	GUIStyle* style
		// Returns:		T* - the casted object (dynamic_cast)
		// Description: Tries to cast the given style to the given type, if the cast fails an error message is printed
		//************************************
		template<typename T>
		T* CastStyleTo(GUIStyle* style, DLib::LOG_TYPE logType = DLib::LOG_TYPE_WARNING)
		{
			T* castStyle = dynamic_cast<T*>(style);

			if(castStyle == nullptr)
				DLib::Logger::LogLine(logType, "Couldn't cast style to " + std::string(typeid(T).name()));

			return castStyle;
		}
	private:
		bool mouseInside;
	};
}

#endif // GUIContainer_h__
