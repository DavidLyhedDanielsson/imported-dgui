#include "list.h"
#include <dlib/input.h>
#include "button.h"

DGUI::List::List()
	: focusOn(nullptr)
	, drawBegin(0)
	, drawEnd(0)
	, elementHeight(0)
	, mouseDown(false)
{
}

DGUI::List::~List()
{
	manager.Clear();
}

void DGUI::List::AddElement(GUIContainer* element)
{
	if(elements.size() > 0)
	{
		GUIContainer* backElement = elements.back();

		element->SetPosition(backElement->GetPosition() + glm::vec2(0.0f, backElement->GetSize().y));
	}
	else
		element->SetPosition(area.GetMinPosition());

	elements.push_back(element);
	manager.AddContainer(element);

	elementHeight = static_cast<int>(elements.back()->GetSize().y);

	scrollbar.SetVisibleItems(static_cast<int>(std::ceil(area.GetHeight() / elementHeight)));
	scrollbar.SetMaxItems(static_cast<unsigned int>(elements.size()));

	UpdatePositions();
}

void DGUI::List::SetElements(const std::vector<DGUI::GUIContainer*>& elements)
{
	this->elements = elements;
	manager.SetContainers(elements);

	if(elements.size() > 0)
	{
		elementHeight = static_cast<int>(elements.back()->GetSize().y);

		scrollbar.SetVisibleItems(static_cast<int>(std::ceil(area.GetHeight() / elements.back()->GetSize().y)));
		scrollbar.SetMaxItems(static_cast<unsigned int>(elements.size()));
	}
	else
	{
		elementHeight = 1;
		scrollbar.SetVisibleItems(0);
		scrollbar.SetMaxItems(0);
	}

	UpdatePositions();
}

void DGUI::List::ClearElements()
{
	elements.clear();
}

void DGUI::List::Init(const DLib::Rect& area, GUIStyle* style, GUIBackground* background, DGUI::GUIStyle* backgroundStyle)
{
	this->style = CastStyleTo<ListStyle>(style);

	GUIContainer::Init(area
					   , style
					   , background
					   , backgroundStyle);

	scrollbar.Init(area
				   , this->style->scrollbarStyle
				   , this->style->scrollbarBackground
				   , this->style->scrollbarBackgroundStyle);
}

void DGUI::List::Update(std::chrono::nanoseconds delta)
{
	manager.Update(delta);
	scrollbar.Update(delta);

	if(mouseDown)
		UpdatePositions();
}

void DGUI::List::Draw(DLib::SpriteRenderer* spriteRenderer)
{
	//spriteRenderer->EnableScissorTest(area);

	DrawBackground(spriteRenderer);
	DrawMiddle(spriteRenderer);
	DrawForeground(spriteRenderer);

	//spriteRenderer->DisableScissorTest();
}

void DGUI::List::DrawBackground(DLib::SpriteRenderer* spriteRenderer)
{
	background->Draw(spriteRenderer);

	for(int i = drawBegin; i < drawEnd; i++)
	{
		GUIContainer* container = elements[i];

		if(container->GetDraw())
			container->DrawBackground(spriteRenderer);
	}
}

void DGUI::List::DrawMiddle(DLib::SpriteRenderer* spriteRenderer)
{
	for(int i = drawBegin; i < drawEnd; i++)
	{
		GUIContainer* container = elements[i];
		if(container->GetDraw())
			container->DrawMiddle(spriteRenderer);
	}
}

void DGUI::List::DrawForeground(DLib::SpriteRenderer* spriteRenderer)
{
	for(int i = drawBegin; i < drawEnd; i++)
	{
		GUIContainer* container = elements[i];
		if(container->GetDraw())
			container->DrawForeground(spriteRenderer);
	}

	scrollbar.Draw(spriteRenderer);
}

void DGUI::List::SetPosition(const glm::vec2& newPosition)
{
	SetPosition(newPosition.x, newPosition.y);
}

void DGUI::List::SetPosition(float x, float y)
{
	GUIContainer::SetPosition(x, y);

	UpdatePositions();
	scrollbar.SetPosition(area.GetMaxPosition().x, area.GetMinPosition().y);
}

void DGUI::List::SetSize(const glm::vec2& newSize)
{
	SetSize(newSize.x, newSize.y);
}

void DGUI::List::SetSize(float x, float y)
{
	GUIContainer::SetSize(x, y);

	UpdatePositions();
	scrollbar.SetSize(scrollbar.GetSize().x, y);
}

void DGUI::List::SetArea(const DLib::Rect& newArea)
{
	SetPosition(newArea.GetMinPosition());
	SetSize(newArea.GetSize());
}

void DGUI::List::OnMouseEnter()
{
	active = true;
}

void DGUI::List::OnMouseExit()
{

}

void DGUI::List::OnMouseDown(const DLib::KeyState& keyState, const glm::vec2& mousePosition)
{
	if(area.Contains(mousePosition))
	{
		mouseDown = true;

		if(!scrollbar.Contains(mousePosition))
		{
			focusOn = nullptr;

			for(GUIContainer* container : elements)
			{
				if(container->GetDraw()
						&& container->GetArea().Contains(mousePosition))
				{
					focusOn = container;
					container->OnMouseDown(keyState, mousePosition);
					break;
				}
			}
		}

		scrollbar.OnMouseDown(keyState, mousePosition);
	}
	else
		active = false;
}

void DGUI::List::OnMouseUp(const DLib::KeyState& keyState, const glm::vec2& mousePosition)
{
	if(focusOn != nullptr)
		focusOn->OnMouseUp(keyState, mousePosition);

	scrollbar.OnMouseUp(keyState, mousePosition);
	mouseDown = false;
}

void DGUI::List::OnKeyDown(const DLib::KeyState& keyState)
{
	if(focusOn != nullptr)
		focusOn->OnKeyDown(keyState);
}

void DGUI::List::OnKeyUp(const DLib::KeyState& keyState)
{
	if(focusOn != nullptr)
		focusOn->OnKeyUp(keyState);
}

void DGUI::List::OnChar(unsigned int keyCode)
{
	if(focusOn != nullptr)
		focusOn->OnChar(keyCode);
}

void DGUI::List::OnScroll(double x, double y)
{
	if(focusOn != nullptr)
		focusOn->OnScroll(x, y);

	scrollbar.OnScroll(x, y);
	UpdatePositions();
}

void DGUI::List::UpdatePositions()
{
	if(elements.size() > 0)
	{
		drawBegin = scrollbar.GetMinIndex();
		drawEnd = scrollbar.GetMaxIndex();

		glm::vec2 newPosition = area.GetMinPosition();

		for(int i = 0, end = drawBegin; i < end; i++)
		{
			elements[i]->SetActive(false);
			elements[i]->SetDraw(false);
		}

		for(int i = drawBegin, end = drawEnd; i < end; i++)
		{
			elements[i]->SetPosition(newPosition);
			elements[i]->SetActive(true);
			elements[i]->SetDraw(true);

			newPosition.y += elementHeight;
		}

		for(int i = drawEnd, end = static_cast<int>(elements.size()); i < end; i++)
		{
			elements[i]->SetActive(false);
			elements[i]->SetDraw(false);
		}
	}
}

void DGUI::List::UpdateSizes()
{
	if(elements.size() > 0)
	{
		scrollbar.SetSize(area.GetMaxPosition().x, area.GetMinPosition().y);
	}
}
