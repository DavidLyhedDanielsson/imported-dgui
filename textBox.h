#ifndef TextBox_h__
#define TextBox_h__

#include "guiContainer.h"

#include <dlib/characterSet.h>
#include <dlib/constructedString.h>

#include "textBoxStyle.h"
#include <dlib/timer.h>

namespace DGUI
{
	class TextBox :
		public GUIContainer
	{
	public:
		TextBox();
		virtual ~TextBox();

		virtual void Init(const DLib::Rect& area, GUIStyle* style, GUIBackground* background, DGUI::GUIStyle* backgroundStyle) override;
		virtual void Update(std::chrono::nanoseconds delta) override;
		//virtual void Draw(DLib::SpriteRenderer* spriteRenderer) override;

		virtual void DrawBackground(DLib::SpriteRenderer* spriteRenderer) override;
		virtual void DrawMiddle(DLib::SpriteRenderer* spriteRenderer) override;
		virtual void DrawForeground(DLib::SpriteRenderer* spriteRenderer) override;

		void Insert(int index, unsigned int character);
		void Insert(unsigned int index, const std::string& text);
		void Insert(unsigned int index, const char* text);
		void Erase(unsigned int startIndex, unsigned int count);

		virtual void SetText(const std::string& text);
		std::string GetText();
		std::string GetSelectedText();

		void SetCharacterSet(const std::string& fontPath, DLib::ContentManager* contentManager);
		virtual void SetCharacterSet(const DLib::CharacterSet* characterSet);
		void SetCutoffString(const std::string& string); //TODO: USE CUTOFF STRING

		virtual void OnMouseEnter() override;
		virtual void OnMouseDown(const DLib::KeyState& keyState, const glm::vec2& mousePosition) override;
		virtual void OnMouseUp(const DLib::KeyState& keyState, const glm::vec2& mousePosition) override;
		virtual void OnKeyDown(const DLib::KeyState& keyState) override;
		virtual void OnKeyUp(const DLib::KeyState& keyState) override;
		virtual void OnChar(unsigned int keyCode) override;

		bool IsEmpty();

		bool allowEdit;
	protected:
		DLib::ConstructedString constructedString;
		DLib::ConstructedString constructedCutoffString;

		TextBoxStyle* style;

		//Used as a general flag. If cursor is drawn then input should be accepted and processed
		bool drawCursor;
		bool mouseDown;

		//////////////////////////////////////////////////////////////////////////
		//CURSOR
		//////////////////////////////////////////////////////////////////////////
		glm::vec4 cursorColor;

		int cursorIndex;

		int selectionIndex;
		int selectionStartIndex;
		int selectionEndIndex;
		
		DLib::Timer cursorBlinkTimer;
		const std::chrono::nanoseconds cursorBlinkTime = std::chrono::milliseconds(750); //Nanoseconds

		float xOffset;

		void LeftPressed(const DLib::KeyState& keyState);
		void RightPressed(const DLib::KeyState& keyState);
		void BackspacePressed(const DLib::KeyState& keyState);
		void DeletePressed(const DLib::KeyState& keyState);

		void SetCursorIndex(unsigned int newIndex);
		void MoveCursorLeft();
		void MoveCursorRight();
		void JumpCursorLeft();
		void JumpCursorRight();

		void ExtendSelectionToCursor();
		void BeginSelection();
		void Deselect();
		void EraseSelection();
		bool SelectionMade();

		void SetXOffset();
	};
}

#endif // TextBox_h__
