#include "guiBackground.h"

DGUI::GUIBackground::GUIBackground()
	: preset(0)
{

}

DGUI::GUIBackground::~GUIBackground()
{

}

void DGUI::GUIBackground::AreaChanged(const DLib::Rect& area)
{
	this->area = area;
}

void DGUI::GUIBackground::ChangePreset(int preset)
{
	this->preset = preset;
}

